/* See LICENSE file for copyright and license details. */

/* appearance */
static unsigned int borderpx  		= 1;        /* border pixel of windows */
static unsigned int snap      		= 32;       /* snap pixel */
static const int swallowfloating    = 0;        /* 1 means swallow floating windows by default */
static int showbar            		= 1;        /* 0 means no bar */
static int topbar             		= 1;        /* 0 means bottom bar */
static const char *fonts[]          = { "JetbrainsMono:size=10", "JoyPixels:pixelsize=10:antialias=true:autohint=true" };
static char dmenufont[]       = "JetbrainsMono:size=10";
static char normbgcolor[]           = "#222222";
static char normbordercolor[]       = "#444444";
static char normfgcolor[]           = "#bbbbbb";
static char selfgcolor[]            = "#eeeeee";
static char selbordercolor[]        = "#005577";
static char selbgcolor[]            = "#005577";
static char statusbarfg[] 			= "#bbbbbb";
static char statusbarbg[] 			= "#222222";
static char seltagbarfg[] 			= "#eeeeee";
static char seltagbarbg[] 			= "#005577";
static char normtagbarfg[] 			= "#bbbbbb";
static char normtagbarbg[] 			= "#222222";
static char selinfobarfg[] 			= "#eeeeee";
static char selinfobarbg[] 			= "#005577";
static char norminfobarfg[] 		= "#bbbbbb";
static char norminfobarbg[] 		= "#222222";

static char col1[]            = "#000000";
static char col2[]            = "#ffffff";
static char col3[]            = "#ffffff";
static char col4[]            = "#ffffff";
static char col5[]            = "#ffffff";
static char col6[]            = "#ffffff";
static char col7[]            = "#ffffff";
static char col8[]            = "#ffffff";
static char col9[]            = "#ffffff";
static char col10[]           = "#ffffff";
static char col11[]           = "#ffffff";
static char col12[]           = "#ffffff";

static const unsigned int baralpha 		= 0x22;
static const unsigned int borderalpha 	= OPAQUE;
static char *colors[][3] = {
       /*               		fg           	bg 				border   */
       	[SchemeNorm] 		= { normfgcolor, 	normbgcolor, 	normbordercolor },
       	[SchemeSel]  		= { selfgcolor,  	selbgcolor,  	selbordercolor  },
		[SchemeStatus]  	= { statusbarfg, 	statusbarbg,  	"#000000"  }, // Statusbar right {text,background,not used but cannot be empty}
		[SchemeTagsSel]  	= { seltagbarfg, 	seltagbarbg,  	"#000000"  }, // Tagbar left selected {text,background,not used but cannot be empty}
		[SchemeTagsNorm]  	= { normtagbarfg, 	normtagbarbg,  	"#000000"  }, // Tagbar left unselected {text,background,not used but cannot be empty}
		[SchemeInfoSel]  	= { selinfobarfg, 	selinfobarbg,  	"#000000"  }, // infobar middle selected {text,background,not used but cannot be empty}
		[SchemeInfoNorm]  	= { norminfobarfg, 	norminfobarbg,  "#000000"  }, // infobar middle unselected {text,background,not used but cannot be empty}
		[SchemeCol1]		  = { col1,      statusbarbg,	normbordercolor },
		[SchemeCol2]		  = { col2,      statusbarbg,	normbordercolor },
		[SchemeCol3]		  = { col3,      statusbarbg,	normbordercolor },
		[SchemeCol4]		  = { col4,      statusbarbg,	normbordercolor },
		[SchemeCol5]		  = { col5,      statusbarbg,	normbordercolor },
		[SchemeCol6]		  = { col6,      statusbarbg,	normbordercolor },
		[SchemeCol7]		  = { col7,      statusbarbg,	normbordercolor },
		[SchemeCol8]		  = { col8,      statusbarbg,	normbordercolor },
		[SchemeCol9]		  = { col9,      statusbarbg,	normbordercolor },
		[SchemeCol10]		  = { col10,     statusbarbg,	normbordercolor },
		[SchemeCol11]		  = { col11,     statusbarbg,	normbordercolor },
		[SchemeCol12]		  = { col12,     statusbarbg,	normbordercolor },
};
static unsigned int alphas[][3] ={
	/* 						fg, 	bg, 	  border*/
	[SchemeNorm] 		= { OPAQUE, baralpha, borderalpha },
	[SchemeSel] 		= { OPAQUE, baralpha, borderalpha },
	[SchemeStatus] 		= { OPAQUE, baralpha, borderalpha },
	[SchemeTagsSel] 	= { OPAQUE, baralpha, borderalpha },
	[SchemeTagsNorm] 	= { OPAQUE, baralpha, borderalpha },
	[SchemeInfoSel] 	= { OPAQUE, baralpha, borderalpha },
	[SchemeInfoNorm] 	= { OPAQUE, baralpha, borderalpha },
};

/* tagging */
static const char *tags[] 		= { "1", "2", "3", "4", "5", "6", "7", "8", "9" };
static const char *tagsalt[] 	= { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {

	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */

	{
		"Gimp", NULL, NULL, 	/* class, instance, title */
		0, -1,        			/* tags mask, monitor */
		1, 1,         			/* isfloating, canfocus */
		0, 0, 			  		/* isalwaysontop,  isfakefullscreen */
		0, 0,       			/* isterminal, noswallow */
		50, 50, 500, 500, 		/* flaot x,y,w,h */
		5 						/* floatborderpx */
	},
	{
		"firefox", NULL, NULL,
		0, -1,
		0, 1,
		0, 1,
		0, 0,
		50, 50, 500, 500,
		5
	},
	{
		"St", NULL, NULL,
		0, -1,
		0, 1,
		0, 0,
		0, 1,
		50, 50, 500, 500,
		50
	},
	{
		"mpv", "gl", NULL,
		0, -1,
		0, 1,
		1, 0,
		0, 1,
		1100, 400, 500, 500,
		0
	},
	{ 	/* xev */
		NULL, NULL, "Event Tester",
		0, -1,
		0, 1,
		0, 0,
		1, 0,
		50, 50, 500, 500,
		5
	},
	{
		NULL, NULL, "Microsoft Teams Notification",
		~0, -1,
		1, 0,
		0, 0,
		0, 0,
		1340, 780, 240, 120,
		0
	},
// 	{
// 		"Microsoft Teams - Preview", "microsoft teams - preview", NULL,
// 		~0, -1,
// 		0, 0,
// 		0, 0,
// 		0, 0,
// 		1340, 780, 240, 120,
// 		0
// 	},
	{
		"firefox", "Toolkit", "Picture-in-Picture",
		~0, -1,
		1, 1,
		1, 0,
		0, 0,
		1280, 720, 480, 270,
		0
	},
	{
		"Dragon-drag-and-drop", "dragon-drag-and-drop", "dragon",
		~0, -1,
		1, 1,
		0, 0,
		0, 0,
		50, 0, 500, 20,
		0
	},
};

/* layout(s) */
static float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static int nmaster     = 1;    /* number of clients in master area */
static int resizehints = 1;    /* 1 means respect size hints in tiled resizals */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
};

// int next_scheme() {
// 	&layouts[
// };

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },
#define STACKKEYS(MOD,ACTION) \
	{ MOD, 		XK_Left, 	ACTION##stack, 		{.i = 0} }, \
	{ MOD, 		XK_h, 		ACTION##stack, 		{.i = 0} }, \
	{ MOD, 		XK_Down, 	ACTION##stack, 		{.i = INC(-1)} }, \
	{ MOD, 		XK_k, 		ACTION##stack, 		{.i = INC(-1)} }, \
	{ MOD, 		XK_j, 		ACTION##stack, 		{.i = INC(+1)} }, \
	{ MOD, 		XK_Up, 		ACTION##stack, 		{.i = INC(+1)} }, \
	{ MOD, 		XK_Right, 	ACTION##stack, 		{.i = -1} }, \
	{ MOD, 		XK_l, 		ACTION##stack, 		{.i = -1} },
#define MONKEYS(MOD,ACTION) \
	{ MOD, 		XK_Left, 	ACTION##mon, 		{.i = 0} }, \
	{ MOD, 		XK_h, 		ACTION##mon, 		{.i = 0} }, \
	{ MOD, 		XK_Down, 	ACTION##mon, 		{.i = INC(-1)} }, \
	{ MOD, 		XK_k, 		ACTION##mon, 		{.i = INC(-1)} }, \
	{ MOD, 		XK_j, 		ACTION##mon, 		{.i = INC(+1)} }, \
	{ MOD, 		XK_Up, 		ACTION##mon, 		{.i = INC(+1)} }, \
	{ MOD, 		XK_Right, 	ACTION##mon, 		{.i = -1} }, \
	{ MOD, 		XK_l, 		ACTION##mon, 		{.i = -1} },
#define LAYOUT(KEY, NUM) \
	{ MODKEY|ShiftMask, 	KEY, 		setlayout, 			{.v = &layouts[NUM]} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, NULL }; //"-fn", dmenufont, "-nb", normbgcolor, "-nf", normfgcolor, "-sb", selbordercolor, "-sf", selfgcolor, NULL };
static const char *termcmd[]  = { "st", NULL };

/* Xresources preferences to load at startup */
ResourcePref resources[] = {
		{ "normbgcolor",        STRING,  	&normbgcolor },
		{ "normbordercolor",    STRING,  	&normbordercolor },
		{ "normfgcolor",        STRING,  	&normfgcolor },
		{ "selbgcolor",         STRING,  	&selbgcolor },
		{ "selbordercolor",     STRING,  	&selbordercolor },
		{ "selfgcolor",         STRING,  	&selfgcolor },
		{ "statusbarfg", 		STRING, 	&statusbarfg },
		{ "statusbarbg" , 		STRING, 	&statusbarbg},
		{ "seltagbarfg" , 		STRING, 	&seltagbarfg},
		{ "seltagbarbg" , 		STRING, 	&seltagbarbg},
		{ "normtagbarfg" , 		STRING, 	&normtagbarfg},
		{ "normtagbarbg" , 		STRING, 	&normtagbarbg},
		{ "selinfobarfg" , 		STRING, 	&selinfobarfg},
		{ "selinfobarbg" , 		STRING, 	&selinfobarbg},
		{ "norminfobarfg" , 	STRING, 	&norminfobarfg},
		{ "norminfobarbg" , 	STRING, 	&norminfobarbg},
		{ "borderpx",          	INTEGER, 	&borderpx },
		{ "snap",          		INTEGER, 	&snap },
		{ "showbar",          	INTEGER, 	&showbar },
		{ "topbar",          	INTEGER, 	&topbar },
		{ "nmaster",          	INTEGER, 	&nmaster },
		{ "resizehints",       	INTEGER, 	&resizehints },
		{ "mfact",      	 	FLOAT,   	&mfact },
};

static Key keys[] = {
	/* modifier                     key        function        argument */
	STACKKEYS(MODKEY,							focus)
	STACKKEYS(MODKEY|ShiftMask,					push)
	MONKEYS(MODKEY|ControlMask,					focus)
	MONKEYS(MODKEY|ShiftMask|ControlMask,		tag)
	{ MODKEY,             			XK_q,      killclient,     {0} },
	{ MODKEY,                       XK_d,      spawn,          {.v = dmenucmd } },
	{ MODKEY,             			XK_Return, spawn,          {.v = termcmd } },
// 	{ MODKEY|ShiftMask,             XK_Return, spawn,          {.v = termcmd } },
	{ MODKEY,                       XK_b,      togglebar,      {0} },
	{ MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_u,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_y,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_o,      setmfact,       {.f = +0.05} },
	{ MODKEY,                       XK_Return, zoom,           {0} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY|ShiftMask,             XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY|ShiftMask,             XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY|ShiftMask,             XK_m,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                       XK_space,  setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	{ MODKEY,             			XK_f,      togglefullscr,  {0} },
	{ MODKEY|ShiftMask|ControlMask, XK_space,  togglealwaysontop, {0} },
	{ MODKEY|ControlMask,           XK_Down,   moveresize,     {.v = "0x 25y 0w 0h" } },
	{ MODKEY|ControlMask,           XK_j,   	moveresize,     {.v = "0x 25y 0w 0h" } },
	{ MODKEY|ControlMask,           XK_Up,     moveresize,     {.v = "0x -25y 0w 0h" } },
	{ MODKEY|ControlMask,           XK_l,     	moveresize,     {.v = "0x -25y 0w 0h" } },
	{ MODKEY|ControlMask,           XK_Right,  moveresize,     {.v = "25x 0y 0w 0h" } },
	{ MODKEY|ControlMask,           XK_l,  		moveresize,     {.v = "25x 0y 0w 0h" } },
	{ MODKEY|ControlMask,           XK_Left,   moveresize,     {.v = "-25x 0y 0w 0h" } },
	{ MODKEY|ControlMask,           XK_h,   	moveresize,     {.v = "-25x 0y 0w 0h" } },
	{ MODKEY|ControlMask|ShiftMask, XK_Down,   moveresize,     {.v = "0x 0y 0w 25h" } },
	{ MODKEY|ControlMask|ShiftMask, XK_j,   	moveresize,     {.v = "0x 0y 0w 25h" } },
	{ MODKEY|ControlMask|ShiftMask, XK_Up,     moveresize,     {.v = "0x 0y 0w -25h" } },
	{ MODKEY|ControlMask|ShiftMask, XK_k,     	moveresize,     {.v = "0x 0y 0w -25h" } },
	{ MODKEY|ControlMask|ShiftMask, XK_Right,  moveresize,     {.v = "0x 0y 25w 0h" } },
	{ MODKEY|ControlMask|ShiftMask, XK_l,  		moveresize,     {.v = "0x 0y 25w 0h" } },
	{ MODKEY|ControlMask|ShiftMask, XK_Left,   moveresize,     {.v = "0x 0y -25w 0h" } },
	{ MODKEY|ControlMask|ShiftMask, XK_h,   	moveresize,     {.v = "0x 0y -25w 0h" } },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
// 	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
// 	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
// 	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
// 	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
	{ MODKEY|ControlMask,           XK_t,      togglealttag,   {0} },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
	LAYOUT( 						XK_F1, 						0)
	LAYOUT( 						XK_F2, 						1)
	LAYOUT( 						XK_F3, 						2)
	{ MODKEY|ControlMask,           XK_Escape, quit,           {0} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button1,        sigdsblocks,    {.i = 1} },
	{ ClkStatusText,        0,              Button2,        sigdsblocks,    {.i = 2} },
	{ ClkStatusText,        0,              Button3,        sigdsblocks,    {.i = 3} },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY|ShiftMask, Button1,      movemouse,      {.i = 1} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkClientWin,         MODKEY|ShiftMask, Button3,      resizemouse,    {.i = 1} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

